import { schedule, Handler } from "@netlify/functions";
import { getCourses } from "../src/parser";
import dotenv from "dotenv";



const handler: Handler = async (event, context) => {
  try {
    dotenv.config();
    getCourses();
    return {
      statusCode: 200,
      body: "Fetching courses into DB ... please wait",
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: "Server Error",
    };
  }
};

module.exports.handler = schedule("@daily", handler);
