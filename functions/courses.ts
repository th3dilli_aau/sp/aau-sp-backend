import { Handler } from "@netlify/functions";
import { courses } from "../src/routes";
import dotenv from "dotenv";

const handler: Handler = async (event, context) => {
  try {
    dotenv.config();
    let data = await courses();
    return {
      statusCode: 200,
      body: JSON.stringify(data),
      headers: {
        "access-control-allow-origin": "*",
      },
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: "Server Error",
    };
  }
};

export { handler };
