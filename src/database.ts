import { Sequelize, Model, DataTypes } from "sequelize";
import dotenv from "dotenv";

dotenv.config();

export interface CourseAttributes {
  id: string;
  type: string;
  title: string;
  professor: string;
  weekDay: string;
  startTime: number;
  endTime: number;
  sws: number;
  semester: string;
}

export class Course extends Model<CourseAttributes> {
  public id!: string;
  public type!: string;
  public title!: string;
  public professor!: string;
  public weekDay!: string;
  public startTime!: number;
  public endTime!: number;
  public sws!: number;
  public semester!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export const db = new Sequelize({
  database: process.env.PGDATABASE || "postgres",
  username: process.env.PGUSER || "postgres",
  password: process.env.PGPASSWORD,
  host: process.env.PGHOST || "localhost",
  dialect: "postgres",
  port: Number(process.env.PGPORT) || 5432,
  logging: false,
});

Course.init(
  {
    id: { type: DataTypes.STRING(8), primaryKey: true },
    type: { type: DataTypes.STRING(2) },
    title: { type: DataTypes.STRING(512) },
    professor: { type: DataTypes.STRING(128) },
    weekDay: { type: DataTypes.STRING(16) },
    startTime: { type: DataTypes.SMALLINT },
    endTime: { type: DataTypes.SMALLINT },
    sws: { type: DataTypes.SMALLINT },
    semester: { type: DataTypes.STRING(3), primaryKey: true },
  },
  { sequelize: db }
);

export function upsert(courses: CourseAttributes[]) {
  Course.bulkCreate(courses, {
    updateOnDuplicate: [
      "type",
      "title",
      "professor",
      "weekDay",
      "startTime",
      "endTime",
      "sws",
    ],
  }).then(
    (res) => {},
    (err) => {
      console.error(err);
    }
  );
}
