import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import { courses, coursesv2, coursesv3 } from "./routes";
import { db } from "./database";
import { getCourses } from "./parser";

dotenv.config();
const app_port = process.env.PORT || 3000;
const app = express();
app.use(cors());

db.authenticate().then(() => {
  app.listen(app_port, () => {
    db.sync();
    console.info("Server started ... listening on port " + app_port);
  });
});

app.get("/courses", async (req, res) => {
  courses()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send("oh snap");
    });
});

app.get("/coursesv2", async (req, res) => {
  coursesv2()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send("oh snap");
    });
});

app.get("/coursesv3", async (req, res) => {
  coursesv3()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send("oh snap");
    });
});

app.get("/getcourses", (req, res) => {
  res.send("Fetching courses into DB ... please wait");
  getCourses();
});
