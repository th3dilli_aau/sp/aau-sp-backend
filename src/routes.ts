import { Sequelize } from "sequelize";
import { Course, CourseAttributes } from "./database";

interface CourseAttributesV1 {
  courseNumber: string;
  type: string;
  title: string;
  prof: string;
  weekDay: string;
  startTime: { hours: number; minutes: number };
  endTime: { hours: number; minutes: number };
  sws: number;
}

export async function courses() {
  return new Promise<CourseAttributesV1[]>((resolve, reject) => {
    Course.findAll({
      attributes: [[Sequelize.literal("distinct semester"), "semester"]],
    }).then(
      (dbSemesters) => {
        let semesters = dbSemesters.map((course) => {
          return course.semester;
        });
        semesters.sort((c1, c2) => {
          if (c1 > c2) {
            return -1;
          } else {
            return 1;
          }
        });
        Course.findAll({ where: { semester: semesters[0] } }).then(
          (data) => {
            resolve(
              data.map((course) => {
                return {
                  courseNumber: course.id,
                  type: course.type,
                  title: course.title,
                  prof: course.professor,
                  weekDay: course.weekDay,
                  startTime: {
                    hours: Math.floor(course.startTime / 60),
                    minutes: course.startTime % 60,
                  },
                  endTime: {
                    hours: Math.floor(course.endTime / 60),
                    minutes: course.endTime % 60,
                  },
                  sws: course.sws,
                };
              })
            );
          },
          (err) => {
            reject(err);
          }
        );
      },
      (error) => {
        reject(error);
      }
    );
  });
}

export function coursesv2() {
  return new Promise<CourseAttributes[]>((resolve, reject) => {
    Course.findAll({
      attributes: [[Sequelize.literal("distinct semester"), "semester"]],
    }).then(
      (dbSemesters) => {
        let semesters = dbSemesters.map((course) => {
          return course.semester;
        });
        semesters.sort((c1, c2) => {
          if (c1 > c2) {
            return -1;
          } else {
            return 1;
          }
        });
        Course.findAll({ where: { semester: semesters[0] } }).then(
          (data) => {
            resolve(
              data.map((course) => {
                return {
                  id: course.id,
                  type: course.type,
                  title: course.title,
                  professor: course.professor,
                  weekDay: course.weekDay,
                  startTime: course.startTime,
                  endTime: course.endTime,
                  sws: course.sws,
                  semester: course.semester,
                };
              })
            );
          },
          (err) => {
            reject(err);
          }
        );
      },
      (error) => {
        reject(error);
      }
    );
  });
}

export function coursesv3() {
  return new Promise<CourseAttributes[]>((resolve, reject) => {
    Course.findAll().then(
      (data) => {
        resolve(
          data.map((course) => {
            return {
              id: course.id,
              type: course.type,
              title: course.title,
              professor: course.professor,
              weekDay: course.weekDay,
              startTime: course.startTime,
              endTime: course.endTime,
              sws: course.sws,
              semester: course.semester,
            };
          })
        );
      },
      (err) => {
        reject(err);
      }
    );
  });
}
