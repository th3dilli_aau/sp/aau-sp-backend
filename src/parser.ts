import axios from "axios";
import { CourseAttributes, upsert } from "./database";
import cheerio, { CheerioAPI, load } from "cheerio";

const courses_urlbase = "https://campus.aau.at/studien/lvliste.jsp?semester=";
const courses_url = "https://campus.aau.at/studien/lvliste.jsp";

export function getCourses() {
  console.info("Fetching courses into DB ... please wait");
  axios.get(courses_url).then((response) => {
    let domObject = load(response.data);
    let newestSemesterRaw = domObject(
      "body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr.bg3box > td:nth-child(2)"
    )
      .text()
      .trim()
      .split(" ");
    let newestSemester = newestSemesterRaw[1] + newestSemesterRaw[0].charAt(0);

    let defaultSemesterRaw = domObject(
      "body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr.bg3box > td.default"
    )
      .text()
      .trim()
      .split(" ");
    let defaultSemester =
      defaultSemesterRaw[1] + defaultSemesterRaw[0].charAt(0);

    let lastSemester = domObject(
      "body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr.bg3box > td:nth-child(3) > a"
    ).attr("href");

    // if new semester is in the page but by default is still the old semester
    if (!lastSemester) {
      let lastSemesterRaw = domObject(
        "body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr.bg3box > td:nth-child(3) > b"
      )
        .text()
        .trim()
        .split(" ");
      lastSemester = lastSemesterRaw[1] + lastSemesterRaw[0].charAt(0);
    } else {
      // case if last semester isnt selected so we have to get semester from href
      //lvliste.jsp?semester=21S
      lastSemester = lastSemester?.split("=")[1];
    }

    let nextSemester: string = "";
    if (newestSemester === defaultSemester) {
      parseCourses(domObject, newestSemester);
      nextSemester = lastSemester;
    } else if (lastSemester === defaultSemester) {
      parseCourses(domObject, lastSemester);
      nextSemester = newestSemester;
    } else {
      console.error(
        `Failed retriving semesters, aborting: newestSemester=${newestSemester} defaultSemester=${defaultSemester} lastSemester=${lastSemester}`
      );
      return;
    }

    if (nextSemester !== "") {
      axios.get(courses_urlbase + nextSemester).then((response) => {
        let domObject = load(response.data);
        console.log("parse " + nextSemester);
        parseCourses(domObject, nextSemester);
      });
    } else {
      console.error("Failed to get Semester: " + lastSemester);
    }
  });
}

export function parseCourses(domObject: CheerioAPI, semester: string) {
  let courses: CourseAttributes[] = [];

  let rows = domObject(
    "body > table.bg4 > tbody > tr:nth-child(2) > td > table > tbody > tr"
  );

  for (let row of rows.toArray()) {
    let current = domObject(row);
    let courseNumber = current.find("td:nth-child(1) > a").text().trim();
    let courseType = current.find("td:nth-child(2)").text().trim();
    let courseTitle = current
      .find("td:nth-child(3) > table > tbody > tr > td > b")
      .text()
      .trim();
    let courseProf = current.find("td:nth-child(5) > a").text().trim();
    let courseTime = current.find("td:nth-child(6)").text();
    let courseSWS = current.find("td:nth-child(7)").text().trim();

    let regex = /([0-9]{2}):([0-9]{2}) - ([0-9]{2}):([0-9]{2})/;

    let out = courseTime.match(regex);
    let startTimeH,
      startTimeMin,
      endTimeH,
      endTimeMin = 0;

    if (out !== null) {
      startTimeH = Number(out[1]);
      startTimeMin = Number(out[2]);
      endTimeH = Number(out[3]);
      endTimeMin = Number(out[4]);
    } else {
      console.error("Error parsing time : " + courseTitle);
      continue;
    }
    let weekDay = "";
    if (courseTime.toLowerCase().includes("montag")) {
      weekDay = "Monday";
    } else if (courseTime.toLowerCase().includes("dienstag")) {
      weekDay = "Tuesday";
    } else if (courseTime.toLowerCase().includes("mittwoch")) {
      weekDay = "Wednesday";
    } else if (courseTime.toLowerCase().includes("donnerstag")) {
      weekDay = "Thursday";
    } else if (courseTime.toLowerCase().includes("freitag")) {
      weekDay = "Friday";
    } else if (courseTime.toLowerCase().includes("samstag")) {
      weekDay = "Saturday";
    } else if (courseTime.toLowerCase().includes("sonntag")) {
      weekDay = "Sunday";
    } else {
      console.error("Error parsing weekday : " + courseTitle);
      continue;
    }
    let course: CourseAttributes = {
      id: courseNumber,
      type: courseType,
      title: courseTitle,
      professor: courseProf,
      weekDay: weekDay,
      startTime: startTimeH * 60 + startTimeMin,
      endTime: endTimeH * 60 + endTimeMin,
      sws: Number(courseSWS),
      semester: semester,
    };
    courses.push(course);
  }
  upsert(courses);
}
